<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO member (nama_member, jenis_kelamin, telepon, password,alamat1, alamat2) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['nama_member'], "text"),
                       GetSQLValueString($_POST['jenis_kelamin'], "text"),
                       GetSQLValueString($_POST['telepon'], "text"),
					   GetSQLValueString($_POST['password'], "text"),
                       GetSQLValueString($_POST['alamat1'], "text"),
                       GetSQLValueString($_POST['alamat2'], "text"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($insertSQL, $koneksi) or die(mysql_error());

  $insertGoTo = "menujadwal.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_koneksi, $koneksi);
$query_jdw = "SELECT * FROM jadwal WHERE tempat = 'Lapangan 1' ORDER BY ID ASC";
$jdw = mysql_query($query_jdw, $koneksi) or die(mysql_error());
$row_jdw = mysql_fetch_assoc($jdw);
$totalRows_jdw = mysql_num_rows($jdw);

mysql_select_db($database_koneksi, $koneksi);
$query_jdw2 = "SELECT * FROM jadwal WHERE tempat = 'Lapangan 2' ORDER BY ID ASC";
$jdw2 = mysql_query($query_jdw2, $koneksi) or die(mysql_error());
$row_jdw2 = mysql_fetch_assoc($jdw2);
$totalRows_jdw2 = mysql_num_rows($jdw2);

mysql_select_db($database_koneksi, $koneksi);
$query_jdw3 = "SELECT * FROM jadwal WHERE tempat = 'Lapangan 3' ORDER BY ID ASC";
$jdw3 = mysql_query($query_jdw3, $koneksi) or die(mysql_error());
$row_jdw3 = mysql_fetch_assoc($jdw3);
$totalRows_jdw3 = mysql_num_rows($jdw3);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000;
	font-weight: bold;
}
body {
	background-color: #000;
	background-attachment: fixed;
	background-repeat: no-repeat;
	background-position: left top;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#EEEEEE">
    <td height="50">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="23%" align="right"><strong>| <a href="index.php" target="_top">Home</a></td>
    <td width="23%" align="right"><h3>&nbsp;</h3></td>
  </tr>
</table>
<br />
<br />
<br />
<br />
<br />
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="TabbedPanels1" class="TabbedPanels">
      <ul class="TabbedPanelsTabGroup">
        <li class="TabbedPanelsTab" tabindex="0">
          <h3>Studio 1</h3>
        </li>
        <li class="TabbedPanelsTab" tabindex="0">
          <h3>Studio 2</h3>
        </li>
        <li class="TabbedPanelsTab" tabindex="0">
          <h3>Studio 3</h3>
        </li>
        <li class="TabbedPanelsTab" tabindex="0">
          <h3>Register Member</h3>
        </li>
      </ul>
      <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent"><br />
          <table width="800" border="10" cellspacing="0" cellpadding="0">
            <tr>
              <td height="40" colspan="7" align="center"><h3>JADWAL PENYEWAAN STUDIO DAN SOUND SYSTEM HARI INI</h3></td>
            </tr>
            <tr>
              <td colspan="7"><img src="images/toolsheaderatas.jpg" width="805" height="25" /></td>
            </tr>
            <tr>
              <td width="50">ID</td>
              <td width="150">JAM</td>
              <td width="150">STUDIO 1</td>
              <td width="150">STATUS</td>
              <td width="150">HARGA PERJAM</td>
              <td width="25" align="center"><img src="images/pdf.png" width="16" height="16" /></td>
              <td width="25" align="center"><img src="images/word.gif" width="16" height="16" /></td>
            </tr>
            <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_jdw['ID']; ?></td>
              <td><?php echo $row_jdw['jam']; ?></td>
              <td><?php echo $row_jdw['tempat']; ?></td>
              <td><?php echo $row_jdw['status_ruangan']; ?></td>
              <td><?php echo $row_jdw['harga_perjam']; ?></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <?php } while ($row_jdw = mysql_fetch_assoc($jdw)); ?>
            <tr>
              <td colspan="7"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
            </tr>
            <tr>
              <td colspan="7">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="TabbedPanelsContent"><br />
          <table width="800" border="10" cellspacing="0" cellpadding="0">
            <tr>
              <td height="40" colspan="7" align="center"><h3>JADWAL PENYEWAAN STUDIO DAN SOUND SYSTEM HARI INI</h3></td>
            </tr>
            <tr>
              <td colspan="7"><img src="images/toolsheaderatas.jpg" width="805" height="25" /></td>
            </tr>
            <tr>
              <td width="50">ID</td>
              <td width="150">JAM</td>
              <td width="150">STUDIO 2</td>
              <td width="150">STATUS</td>
              <td width="150">HARGA PERJAM</td>
              <td width="25" align="center"><img src="images/pdf.png" width="16" height="16" /></td>
              <td width="25" align="center"><img src="images/word.gif" width="16" height="16" /></td>
            </tr>
            <?php do { ?>
                <?php do { ?>
                  <tr bgcolor="#FFFFFF">
                    <td><?php echo $row_jdw2['ID']; ?></td>
                    <td><?php echo $row_jdw2['jam']; ?></td>
                    <td><?php echo $row_jdw2['tempat']; ?></td>
                    <td><?php echo $row_jdw2['status_ruangan']; ?></td>
                    <td><?php echo $row_jdw2['harga_perjam']; ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php } while ($row_jdw = mysql_fetch_assoc($jdw)); ?>
                <?php } while ($row_jdw2 = mysql_fetch_assoc($jdw2)); ?>
<tr>
              <td colspan="7"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
            </tr>
            <tr>
              <td colspan="7">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="TabbedPanelsContent"><br />
          <table width="800" border="10" cellspacing="0" cellpadding="0">
            <tr>
              <td height="40" colspan="7" align="center"><h3>JADWAL PENYEWAAN STUDIO DAN SOUND SYSTEM HARI INI</h3></td>
            </tr>
            <tr>
              <td colspan="7"><img src="images/toolsheaderatas.jpg" width="805" height="25" /></td>
            </tr>
            <tr>
              <td width="50">ID</td>
              <td width="150">JAM</td>
              <td width="150">STUDIO 3</td>
              <td width="150">STATUS</td>
              <td width="150">HARGA PERJAM</td>
              <td width="25" align="center"><img src="images/pdf.png" width="16" height="16" /></td>
              <td width="25" align="center"><img src="images/word.gif" width="16" height="16" /></td>
            </tr>
            <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_jdw3['ID']; ?></td>
              <td><?php echo $row_jdw3['jam']; ?></td>
              <td><?php echo $row_jdw3['tempat']; ?></td>
              <td><?php echo $row_jdw3['status_ruangan']; ?></td>
              <td><?php echo $row_jdw3['harga_perjam']; ?></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <?php } while ($row_jdw3 = mysql_fetch_assoc($jdw3)); ?>
            <tr>
              <td colspan="7"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
            </tr>
            <tr>
              <td colspan="7">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="TabbedPanelsContent">Register Member<br />
          <form id="form1" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
            <table width="800" border="1" cellspacing="0" cellpadding="0">
              <tr>
                <td height="35" colspan="2" align="center"><h2>REGISTER MENJADI MEMBER</h2></td>
                </tr>
              <tr>
                <td colspan="2"><img src="images/toolsheaderatas.jpg" width="805" height="25" /></td>
                </tr>
              <tr>
                <td width="200">Kode Member</td>
                <td width="600">Auto</td>
                </tr>
              <tr>
                <td>Nama Member</td>
                <td><span id="sprytextfield1">
                  <label>
                    <input name="nama_member" type="text" id="nama_member" size="25" />
                  </label>
                  <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td><span id="spryselect1">
                  <label>
                    <select name="jenis_kelamin" id="jenis_kelamin">
                      <option>Pilih Jenis Kelamin</option>
                      <option value="Laki-Laki">Laki-Laki</option>
                      <option value="Perempuan">Perempuan</option>
                    </select>
                  </label>
                  <span class="selectRequiredMsg">Please select an item.</span></span></td>
                </tr>
              <tr>
                <td>Telepon</td>
                <td><span id="sprytextfield2">
                  <label>
                    <input type="text" name="telepon" id="telepon" />
                  </label>
                  <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                </tr>
				<tr>
                <td>Password</td>
                <td><span id="sprytextfield2">
                  <label>
                    <input type="text" name="password" id="password" />
                  </label>
                  <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                </tr>
              <tr>
                <td>Alamat</td>
                <td><span id="sprytextfield3">
                  <label>
                    <input name="alamat1" type="text" id="alamat1" size="50" />
                  </label>
                  <span class="textfieldRequiredMsg">A value is required.</span></span></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><span id="sprytextfield4">
                  <label>
                    <input name="alamat2" type="text" id="alamat2" size="45" />
                  </label>
                  <span class="textfieldRequiredMsg">A value is required.</span></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><label>
                  <input type="submit" name="button" id="button" value="Simpan" />
                </label></td>
                </tr>
              <tr>
                <td colspan="2"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
                </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
                </tr>
            </table>
            <input type="hidden" name="MM_insert" value="form1" />
          </form>
        </div>
      </div>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($jdw);

mysql_free_result($jdw2);

mysql_free_result($jdw3);
?>
