<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$maxRows_pny = 10;
$pageNum_pny = 0;
if (isset($_GET['pageNum_pny'])) {
  $pageNum_pny = $_GET['pageNum_pny'];
}
$startRow_pny = $pageNum_pny * $maxRows_pny;

mysql_select_db($database_koneksi, $koneksi);
$query_pny = "SELECT * FROM penyewaan ORDER BY ID ASC";
$query_limit_pny = sprintf("%s LIMIT %d, %d", $query_pny, $startRow_pny, $maxRows_pny);
$pny = mysql_query($query_limit_pny, $koneksi) or die(mysql_error());
$row_pny = mysql_fetch_assoc($pny);

if (isset($_GET['totalRows_pny'])) {
  $totalRows_pny = $_GET['totalRows_pny'];
} else {
  $all_pny = mysql_query($query_pny);
  $totalRows_pny = mysql_num_rows($all_pny);
}
$totalPages_pny = ceil($totalRows_pny/$maxRows_pny)-1;

$colname_LDP = "-1";
if (isset($_POST['nama_member'])) {
  $colname_LDP = $_POST['nama_member'];
}
mysql_select_db($database_koneksi, $koneksi);
$query_LDP = sprintf("SELECT * FROM penyewaan WHERE nama_member LIKE %s", GetSQLValueString("%" . $colname_LDP . "%", "text"));
$LDP = mysql_query($query_LDP, $koneksi) or die(mysql_error());
$row_LDP = mysql_fetch_assoc($LDP);
$totalRows_LDP = mysql_num_rows($LDP);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Laporan Data Penyewaan</li>
    <li class="TabbedPanelsTab" tabindex="0">Laporan Per Member</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Lihat Cabang<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="718" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="6" align="center" bgcolor="#EEEEEE"><h2>DATA PENYEWAAN</h2></td>
          </tr>
          <tr>
            <td colspan="6" bgcolor="#CCCCCC">&nbsp;</td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="118">TANGGAL</td>
            <td width="250">NAMA PENYEWA</td>
            <td width="100">RUANGAN</td>
            <td width="100">HARGA PERJAM</td>
            <td width="100"><p>NO TELEPON</p></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_pny['ID']; ?></td>
              <td><?php echo $row_pny['tanggal']; ?></td>
              <td><?php echo $row_pny['nama_member']; ?></td>
              <td><?php echo $row_pny['nama_tempat']; ?></td>
              <td><?php echo $row_pny['harga_perjam']; ?></td>
              <td><?php echo $row_pny['no_telepon']; ?></td>
            </tr>
            <?php } while ($row_pny = mysql_fetch_assoc($pny)); ?>
<tr>
            <td colspan="6"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="3">Data Penyewa <?php echo ($startRow_pny + 1) ?> to <?php echo min($startRow_pny + $maxRows_pny, $totalRows_pny) ?> of <?php echo $totalRows_pny ?></td>
            <td colspan="3" align="right">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
    <div class="TabbedPanelsContent">&gt;&gt; Data Penyewaan<br />
      <form action="laporan_penyewaan_cari.php" method="post" name="form2" target="_self" id="form2">
        <table width="718" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="6" align="center" bgcolor="#EEEEEE"><h2>DATA PENYEWAAN</h2></td>
          </tr>
          <tr>
            <td colspan="6">Berdasarkan Nama 
              <label>
                <input type="text" name="nama_member" id="nama_member" />
                <input type="submit" name="button" id="button" value="Search" />
            </label></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="118">TANGGAL</td>
            <td width="250">NAMA PENYEWA</td>
            <td width="100">LAPANGAN</td>
            <td width="100">HARGA PERJAM</td>
            <td width="100"><p>NO TELEPON</p></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_LDP['ID']; ?></td>
              <td><?php echo $row_LDP['tanggal']; ?></td>
              <td><?php echo $row_LDP['nama_member']; ?></td>
              <td><?php echo $row_LDP['nama_tempat']; ?></td>
              <td><?php echo $row_LDP['harga_perjam']; ?></td>
              <td><?php echo $row_LDP['no_telepon']; ?></td>
            </tr>
            <?php } while ($row_LDP = mysql_fetch_assoc($LDP)); ?>
<tr>
            <td colspan="6"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="3">&nbsp;</td>
            <td colspan="3" align="right">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:1});
//-->
</script>
</body>
</html>
<?php
mysql_free_result($pny);

mysql_free_result($LDP);
?>
