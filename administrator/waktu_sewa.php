<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO jam_sewa (kode_jam_sewa, jam_sewa) VALUES (%s, %s)",
                       GetSQLValueString($_POST['kode_jam_sewa'], "int"),
                       GetSQLValueString($_POST['jam_sewa'], "text"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($insertSQL, $koneksi) or die(mysql_error());

  $insertGoTo = "waktu_sewa.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$maxRows_dws = 50;
$pageNum_dws = 0;
if (isset($_GET['pageNum_dws'])) {
  $pageNum_dws = $_GET['pageNum_dws'];
}
$startRow_dws = $pageNum_dws * $maxRows_dws;

mysql_select_db($database_koneksi, $koneksi);
$query_dws = "SELECT * FROM jam_sewa ORDER BY kode_jam_sewa ASC";
$query_limit_dws = sprintf("%s LIMIT %d, %d", $query_dws, $startRow_dws, $maxRows_dws);
$dws = mysql_query($query_limit_dws, $koneksi) or die(mysql_error());
$row_dws = mysql_fetch_assoc($dws);

if (isset($_GET['totalRows_dws'])) {
  $totalRows_dws = $_GET['totalRows_dws'];
} else {
  $all_dws = mysql_query($query_dws);
  $totalRows_dws = mysql_num_rows($all_dws);
}
$totalPages_dws = ceil($totalRows_dws/$maxRows_dws)-1;

$queryString_dws = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_dws") == false && 
        stristr($param, "totalRows_dws") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_dws = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_dws = sprintf("&totalRows_dws=%d%s", $totalRows_dws, $queryString_dws);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BANK OCBC</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Input Data</li>
    <li class="TabbedPanelsTab" tabindex="0">Lihat Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Input Data Sewa<br />
      <form id="form2" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>INPUT DATA JAM SEWA</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">KODE</td>
            <td width="250">NAMA JAM SEWA</td>
            <td width="200"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td><span id="sprytextfield1">
              <label>
                <input name="kode_jam_sewa" type="text" id="kode_jam_sewa" size="15" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><span id="sprytextfield2">
              <label>
                <input name="jam_sewa" type="text" id="jam_sewa" size="35" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><label>
              <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
            <td align="center">Edit</td>
            <td align="center">Del</td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
        <input type="hidden" name="MM_insert" value="form1" />
      </form>
    </div>
    <div class="TabbedPanelsContent">&gt;&gt; Lihat Data<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>DATA WAKTU SEWA</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="250">NAMA WAKTU SEWA</td>
            <td width="200"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_dws['kode_jam_sewa']; ?></td>
              <td><?php echo $row_dws['jam_sewa']; ?></td>
              <td>&nbsp;</td>
              <td align="center"><a href="waktu_sewa_edit.php?kode_jam_sewa=<?php echo $row_dws['kode_jam_sewa']; ?>">Edit</a></td>
              <td align="center"><a href="waktu_sewa_delete.php?kode_jam_sewa=<?php echo $row_dws['kode_jam_sewa']; ?>">Del</a></td>
            </tr>
            <?php } while ($row_dws = mysql_fetch_assoc($dws)); ?>
<tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3" align="right">&nbsp;
              <table border="0">
                <tr>
                  <td><?php if ($pageNum_dws > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_dws=%d%s", $currentPage, 0, $queryString_dws); ?>"><img src="First.gif" /></a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_dws > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_dws=%d%s", $currentPage, max(0, $pageNum_dws - 1), $queryString_dws); ?>"><img src="Previous.gif" /></a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_dws < $totalPages_dws) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_dws=%d%s", $currentPage, min($totalPages_dws, $pageNum_dws + 1), $queryString_dws); ?>"><img src="Next.gif" /></a>
                      <?php } // Show if not last page ?></td>
                  <td><?php if ($pageNum_dws < $totalPages_dws) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_dws=%d%s", $currentPage, $totalPages_dws, $queryString_dws); ?>"><img src="Last.gif" /></a>
                      <?php } // Show if not last page ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
      </form>
    </div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:1});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($dws);
?>
