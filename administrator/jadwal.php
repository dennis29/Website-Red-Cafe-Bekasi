<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO jadwal (jam, tempat, status_ruangan, harga_perjam, keterangan) VALUES (%s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['jam'], "text"),
                       GetSQLValueString($_POST['tempat'], "text"),
                       GetSQLValueString($_POST['status_ruangan'], "text"),
                       GetSQLValueString($_POST['harga_perjam'], "int"),
                       GetSQLValueString($_POST['keterangan'], "text"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($insertSQL, $koneksi) or die(mysql_error());

  $insertGoTo = "jadwal.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

mysql_select_db($database_koneksi, $koneksi);
$query_jdw = "SELECT * FROM jadwal ORDER BY ID ASC";
$jdw = mysql_query($query_jdw, $koneksi) or die(mysql_error());
$row_jdw = mysql_fetch_assoc($jdw);
$totalRows_jdw = mysql_num_rows($jdw);

mysql_select_db($database_koneksi, $koneksi);
$query_PilihJam = "SELECT jam_sewa FROM jam_sewa";
$PilihJam = mysql_query($query_PilihJam, $koneksi) or die(mysql_error());
$row_PilihJam = mysql_fetch_assoc($PilihJam);
$totalRows_PilihJam = mysql_num_rows($PilihJam);

mysql_select_db($database_koneksi, $koneksi);
$query_PilihRuangan = "SELECT * FROM tempat";
$PilihRuangan = mysql_query($query_PilihRuangan, $koneksi) or die(mysql_error());
$row_PilihRuangan = mysql_fetch_assoc($PilihRuangan);
$totalRows_PilihRuangan = mysql_num_rows($PilihRuangan);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body> 
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Lihat Jadwal</li>
    <li class="TabbedPanelsTab" tabindex="0">Input Jadwal</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Lihat Jadwal<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="850" border="10">
          <tr>
            <td height="40" colspan="8" align="center"><h3>JADWAL PENYEWAAN RUANGAN HARI INI</h3></td>
          </tr>
          <tr>
            <td colspan="8"><img src="images/toolsheaderatas.jpg" width="850" height="25" /></td>
          </tr>
          <tr>
            <td width="50">ID</td>
            <td width="200">JAM</td>
            <td width="200">STUDIO</td>
            <td width="200">STATUS</td>
            <td width="300">HARGA PERJAM</td>
			<td width="300">KETERANGAN</td>
            <td width="50" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="50" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <?php do { ?>
          <tr bgcolor="#FFFFFF">
            <td><?php echo $row_jdw['ID']; ?></td>
            <td><?php echo $row_jdw['jam']; ?></td>
            <td><?php echo $row_jdw['tempat']; ?></td>
            <td><?php echo $row_jdw['status_ruangan']; ?></td>
            <td><?php echo $row_jdw['harga_perjam']; ?></td>
			<td><?php echo $row_jdw['keterangan']; ?></td>
            <td align="center"><a href="jadwaledit.php?ID=<?php echo $row_jdw['ID']; ?>">Edit</a></td>
            <td align="center"><a href="jadwaldelete.php?ID=<?php echo $row_jdw['ID']; ?>">Del</a></td>
          </tr>
          <?php } while ($row_jdw = mysql_fetch_assoc($jdw)); ?>
          <tr>
            <td colspan="8"><img src="images/toolsheaderbawah.jpg" width="850" height="30" /></td>
          </tr>
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
    <div class="TabbedPanelsContent">&gt;&gt; Input Jadwal<br />
      <form id="form2" name="form2" method="POST" action="<?php echo $editFormAction; ?>">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="3" align="center" bgcolor="#EEEEEE"><h2>INPUT DATA JADWAL</h2></td>
          </tr>
          <tr>
            <td colspan="3"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">ITEM</td>
            <td width="250">ISIAN</td>
            <td width="200"><p>KETERANGAN</p></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>ID</td>
            <td>Auto</td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>JAM</td>
            <td><span id="spryselect1">
              <label>
                <select name="jam" id="jam">
                  <option value="">Pilih Jam</option>
                  <?php
do {  
?>
                  <option value="<?php echo $row_PilihJam['jam_sewa']?>"><?php echo $row_PilihJam['jam_sewa']?></option>
                  <?php
} while ($row_PilihJam = mysql_fetch_assoc($PilihJam));
  $rows = mysql_num_rows($PilihJam);
  if($rows > 0) {
      mysql_data_seek($PilihJam, 0);
	  $row_PilihJam = mysql_fetch_assoc($PilihJam);
  }
?>
                </select>
              </label>
            <span class="selectRequiredMsg">Please select an item.</span></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>STUDIO</td>
            <td><span id="spryselect2">
              <label>
                <select name="tempat" id="tempat" onchange="submitini(this.value)" onclick="submitini(this.value)">
                  <option value="">Pilih Ruangan</option>
                  <?php
				  //$harga_perjam="0";
do {  
?>
                  <option value="<?php $harga_perjam['0']=$row_PilihRuangan['harga_perjam'].'^[]$'.$row_PilihRuangan['nama_tempat'];
				  echo $harga_perjam['0'];?>"><?php echo $row_PilihRuangan['nama_tempat']?></option>
                  <?php
} while ($row_PilihRuangan = mysql_fetch_assoc($PilihRuangan));
  $rows = mysql_num_rows($PilihRuangan);
  if($rows > 0) {
      mysql_data_seek($PilihRuangan, 0);
	  $row_PilihRuangan = mysql_fetch_assoc($PilihRuangan);
  }
?>
                </select>
              </label>
            <span class="selectRequiredMsg">Please select an item.</span></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>STATUS </td>
            <td><span id="spryselect3">
              <label>
                <select name="status_ruangan" id="status_ruangan">
                  <option>Pilih Status Ruangan</option>
                  <option value="Terisi">Terisi</option>
                  <option value="Kosong">Kosong</option>
                </select>
              </label>
            <span class="selectRequiredMsg">Please select an item.</span></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>HARGA</td>
            <td><span id="sprytextfield1">
              <label>
                <input name="harga_perjam" type="text" id="harga_perjam" size="12" readonly = readonly/>
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>KETERANGAN</td>
            <td><label>
              <input name="keterangan" type="text" id="keterangan" size="30" />
            </label></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>&nbsp;</td>
            <td><label>
              <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
        <input type="hidden" name="MM_insert" value="form1" />

<script> function submitini(nilai){
document.forms[1].harga_perjam.value=nilai.split("^[]$")[0];document.forms[1].tempat.value=nilai.split("-")[1];
}
//form2.harga_perjam.value='lkllkl';</script>		
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2");
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($jdw);

mysql_free_result($PilihJam);

mysql_free_result($PilihRuangan);
?>
