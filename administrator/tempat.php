<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO tempat (kode_tempat, nama_tempat, harga_perjam) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['kode_tempat'], "text"),
                       GetSQLValueString($_POST['nama_tempat'], "text"),
                       GetSQLValueString($_POST['harga_perjam'], "int"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($insertSQL, $koneksi) or die(mysql_error());

  $insertGoTo = "tempat.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$maxRows_TMP = 10;
$pageNum_TMP = 0;
if (isset($_GET['pageNum_TMP'])) {
  $pageNum_TMP = $_GET['pageNum_TMP'];
}
$startRow_TMP = $pageNum_TMP * $maxRows_TMP;

mysql_select_db($database_koneksi, $koneksi);
$query_TMP = "SELECT * FROM tempat ORDER BY kode_tempat ASC";
$query_limit_TMP = sprintf("%s LIMIT %d, %d", $query_TMP, $startRow_TMP, $maxRows_TMP);
$TMP = mysql_query($query_limit_TMP, $koneksi) or die(mysql_error());
$row_TMP = mysql_fetch_assoc($TMP);

if (isset($_GET['totalRows_TMP'])) {
  $totalRows_TMP = $_GET['totalRows_TMP'];
} else {
  $all_TMP = mysql_query($query_TMP);
  $totalRows_TMP = mysql_num_rows($all_TMP);
}
$totalPages_TMP = ceil($totalRows_TMP/$maxRows_TMP)-1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Input Data</li>
    <li class="TabbedPanelsTab" tabindex="0">Lihat Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Input Data <br />
      <form action="<?php echo $editFormAction; ?>" method="POST" name="form1" target="_self" id="form2">
        <table width="814" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="6" align="center" bgcolor="#EEEEEE"><h2>INPUT DATA TEMPAT</h2></td>
          </tr>
          <tr>
            <td colspan="6"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">KODE</td>
            <td width="250">NAMA STUDIO</td>
            <td width="100">HARGA</td>
            <td width="100"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td><span id="sprytextfield1">
              <label>
                <input name="kode_tempat" type="text" id="kode_tempat" size="20" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><span id="sprytextfield2">
              <label>
                <input name="nama_tempat" type="text" id="nama_tempat" size="35" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><label>
              <input type="text" name="harga_perjam" id="harga_perjam" />
            </label></td>
            <td><label>
              <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
            <td align="center">Edit</td>
            <td align="center">Del</td>
          </tr>
          <tr>
            <td colspan="6"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
        </table>
        <input type="hidden" name="MM_insert" value="form1" />
      </form>
    </div>
    <div class="TabbedPanelsContent">&gt;&gt; Lihat Data<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>DATA TEMPAT</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="250">NAMA STUDIO</td>
            <td width="200"><p>HARGA</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_TMP['kode_tempat']; ?></td>
              <td><?php echo $row_TMP['nama_tempat']; ?></td>
              <td><?php echo $row_TMP['harga_perjam']; ?></td>
              <td align="center"><a href="tempatedit.php?kode_tempat=<?php echo $row_TMP['kode_tempat']; ?>">Edit</a></td>
              <td align="center"><a href="tempatdelete.php?kode_tempat=<?php echo $row_TMP['kode_tempat']; ?>">Del</a></td>
            </tr>
            <?php } while ($row_TMP = mysql_fetch_assoc($TMP)); ?>
<tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:1});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($TMP);
?>
