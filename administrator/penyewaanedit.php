<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE penyewaan SET tanggal=%s, nama_member=%s, no_telepon=%s, nama_tempat=%s, lama_sewa=%s, mulai_jam=%s, sampai_jam=%s, harga_perjam=%s, nama_jenis_pembayaran=%s, keterangan=%s WHERE ID=%s",
                       GetSQLValueString($_POST['tanggal'], "text"),
                       GetSQLValueString($_POST['nama_member'], "text"),
                       GetSQLValueString($_POST['no_telepon'], "text"),
                       GetSQLValueString($_POST['nama_tempat'], "text"),
                       GetSQLValueString($_POST['lama_sewa'], "int"),
                       GetSQLValueString($_POST['mulai_jam'], "text"),
                       GetSQLValueString($_POST['sampai_jam'], "text"),
                       GetSQLValueString($_POST['harga_perjam'], "int"),
                       GetSQLValueString($_POST['nama_jenis_pembayaran'], "text"),
                       GetSQLValueString($_POST['keterangan'], "text"),
                       GetSQLValueString($_POST['ID'], "int"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($updateSQL, $koneksi) or die(mysql_error());

  $updateGoTo = "penyewaan.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_sewa = 10;
$pageNum_sewa = 0;
if (isset($_GET['pageNum_sewa'])) {
  $pageNum_sewa = $_GET['pageNum_sewa'];
}
$startRow_sewa = $pageNum_sewa * $maxRows_sewa;

$colname_sewa = "-1";
if (isset($_GET['ID'])) {
  $colname_sewa = $_GET['ID'];
}
mysql_select_db($database_koneksi, $koneksi);
$query_sewa = sprintf("SELECT * FROM penyewaan WHERE ID = %s", GetSQLValueString($colname_sewa, "int"));
$query_limit_sewa = sprintf("%s LIMIT %d, %d", $query_sewa, $startRow_sewa, $maxRows_sewa);
$sewa = mysql_query($query_limit_sewa, $koneksi) or die(mysql_error());
$row_sewa = mysql_fetch_assoc($sewa);

if (isset($_GET['totalRows_sewa'])) {
  $totalRows_sewa = $_GET['totalRows_sewa'];
} else {
  $all_sewa = mysql_query($query_sewa);
  $totalRows_sewa = mysql_num_rows($all_sewa);
}
$totalPages_sewa = ceil($totalRows_sewa/$maxRows_sewa)-1;

mysql_select_db($database_koneksi, $koneksi);
$query_PilihRuangan = "SELECT nama_tempat FROM tempat";
$PilihRuangan = mysql_query($query_PilihRuangan, $koneksi) or die(mysql_error());
$row_PilihRuangan = mysql_fetch_assoc($PilihRuangan);
$totalRows_PilihRuangan = mysql_num_rows($PilihRuangan);

mysql_select_db($database_koneksi, $koneksi);
$query_PilihPembayaran = "SELECT nama_jenis_pembayaran FROM jenis_pembayaran";
$PilihPembayaran = mysql_query($query_PilihPembayaran, $koneksi) or die(mysql_error());
$row_PilihPembayaran = mysql_fetch_assoc($PilihPembayaran);
$totalRows_PilihPembayaran = mysql_num_rows($PilihPembayaran);

$queryString_sewa = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_sewa") == false && 
        stristr($param, "totalRows_sewa") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_sewa = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_sewa = sprintf("&totalRows_sewa=%d%s", $totalRows_sewa, $queryString_sewa);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
<li class="TabbedPanelsTab" tabindex="0">Input Data Penyewaan</li>
  </ul>
  <div class="TabbedPanelsContentGroup">
<div class="TabbedPanelsContent">&gt;&gt; Input Data Cabang<br />
      <form action="<?php echo $editFormAction; ?>" id="form2" name="form1" method="POST">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="2" align="center" bgcolor="#EEEEEE"><h2>INPUT DATA PENYEWAAN</h2></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">ITEM</td>
            <td width="250">NAMA CABANG</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>ID</td>
            <td><label>
              <input name="ID" type="text" id="ID" value="<?php echo $row_sewa['ID']; ?>" size="5" />
            </label></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Tanggal</td>
            <td><span id="sprytextfield1">
              <label>
                <input name="tanggal" type="text" id="tanggal" value="<?php echo $row_sewa['tanggal']; ?>" size="15" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span>Thn-Bln-Tgl</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Nama Penyewa</td>
            <td><span id="sprytextfield3">
              <label>
                <input name="nama_member" type="text" id="nama_member" value="<?php echo $row_sewa['nama_member']; ?>" size="35" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>No Telepon</td>
            <td><span id="sprytextfield4">
              <label>
                <input name="no_telepon" type="text" id="no_telepon" value="<?php echo $row_sewa['no_telepon']; ?>" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Ruangan</td>
            <td><span id="spryselect1">
              <label>
                <select name="nama_tempat" id="nama_tempat">
                  <option value="" <?php if (!(strcmp("", $row_sewa['nama_tempat']))) {echo "selected=\"selected\"";} ?>>Pilih</option>
                  <?php
do {  
?>
<option value="<?php echo $row_PilihRuangan['nama_tempat']?>"<?php if (!(strcmp($row_PilihRuangan['nama_tempat'], $row_sewa['nama_tempat']))) {echo "selected=\"selected\"";} ?>><?php echo $row_PilihRuangan['nama_tempat']?></option>
                  <?php
} while ($row_PilihRuangan = mysql_fetch_assoc($PilihRuangan));
  $rows = mysql_num_rows($PilihRuangan);
  if($rows > 0) {
      mysql_data_seek($PilihRuangan, 0);
	  $row_PilihRuangan = mysql_fetch_assoc($PilihRuangan);
  }
?>
                </select>
              </label>
            <span class="selectRequiredMsg">Please select an item.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Lama Sewa</td>
            <td><span id="sprytextfield5">
              <label>
                <input name="lama_sewa" type="text" id="lama_sewa" value="<?php echo $row_sewa['lama_sewa']; ?>" size="10" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span>Jam</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Mulai Jam</td>
            <td><span id="sprytextfield6">
              <label>
                <input name="mulai_jam" type="text" id="mulai_jam" value="<?php echo $row_sewa['mulai_jam']; ?>" size="10" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Sampai Jam</td>
            <td><span id="sprytextfield7">
              <label>
                <input name="sampai_jam" type="text" id="sampai_jam" value="<?php echo $row_sewa['sampai_jam']; ?>" size="10" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Harga Sewa Perjam</td>
            <td><span id="sprytextfield8">
              <label>
                <input name="harga_perjam" type="text" id="harga_perjam" value="<?php echo $row_sewa['harga_perjam']; ?>" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Jenis Pembayaran</td>
            <td><span id="spryselect3">
              <label>
                <select name="nama_jenis_pembayaran" id="nama_jenis_pembayaran">
                  <option value="" <?php if (!(strcmp("", $row_sewa['nama_jenis_pembayaran']))) {echo "selected=\"selected\"";} ?>>Pilih Jenis Pembayaran</option>
                  <?php
do {  
?>
<option value="<?php echo $row_PilihPembayaran['nama_jenis_pembayaran']?>"<?php if (!(strcmp($row_PilihPembayaran['nama_jenis_pembayaran'], $row_sewa['nama_jenis_pembayaran']))) {echo "selected=\"selected\"";} ?>><?php echo $row_PilihPembayaran['nama_jenis_pembayaran']?></option>
                  <?php
} while ($row_PilihPembayaran = mysql_fetch_assoc($PilihPembayaran));
  $rows = mysql_num_rows($PilihPembayaran);
  if($rows > 0) {
      mysql_data_seek($PilihPembayaran, 0);
	  $row_PilihPembayaran = mysql_fetch_assoc($PilihPembayaran);
  }
?>
                </select>
              </label>
            <span class="selectRequiredMsg">Please select an item.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>Keterangan</td>
            <td><span id="sprytextfield2">
              <label>
                <input name="keterangan" type="text" id="keterangan" value="<?php echo $row_sewa['keterangan']; ?>" size="35" />
              </label>
            <span class="textfieldRequiredMsg">A value is required.</span></span></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>&nbsp;</td>
            <td><label>
                <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
        <input type="hidden" name="MM_update" value="form1" />
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7");
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8");
var spryselect3 = new Spry.Widget.ValidationSelect("spryselect3");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($sewa);

mysql_free_result($PilihRuangan);

mysql_free_result($PilihPembayaran);
?>
