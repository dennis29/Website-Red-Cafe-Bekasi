<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['kode_jam_sewa'])) && ($_GET['kode_jam_sewa'] != "")) {
  $deleteSQL = sprintf("DELETE FROM jam_sewa WHERE kode_jam_sewa=%s",
                       GetSQLValueString($_GET['kode_jam_sewa'], "int"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($deleteSQL, $koneksi) or die(mysql_error());

  $deleteGoTo = "waktu_sewa.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_dws = 10;
$pageNum_dws = 0;
if (isset($_GET['pageNum_dws'])) {
  $pageNum_dws = $_GET['pageNum_dws'];
}
$startRow_dws = $pageNum_dws * $maxRows_dws;

$colname_dws = "-1";
if (isset($_GET['kode_jam_sewa'])) {
  $colname_dws = $_GET['kode_jam_sewa'];
}
mysql_select_db($database_koneksi, $koneksi);
$query_dws = sprintf("SELECT * FROM jam_sewa WHERE kode_jam_sewa = %s", GetSQLValueString($colname_dws, "int"));
$query_limit_dws = sprintf("%s LIMIT %d, %d", $query_dws, $startRow_dws, $maxRows_dws);
$dws = mysql_query($query_limit_dws, $koneksi) or die(mysql_error());
$row_dws = mysql_fetch_assoc($dws);

if (isset($_GET['totalRows_dws'])) {
  $totalRows_dws = $_GET['totalRows_dws'];
} else {
  $all_dws = mysql_query($query_dws);
  $totalRows_dws = mysql_num_rows($all_dws);
}
$totalPages_dws = ceil($totalRows_dws/$maxRows_dws)-1;

$queryString_dws = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_dws") == false && 
        stristr($param, "totalRows_dws") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_dws = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_dws = sprintf("&totalRows_dws=%d%s", $totalRows_dws, $queryString_dws);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Rental</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Delete Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Edit Data<br />
      <form id="form2" name="form1" method="POST">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>DELETE DATA JAM SEWA</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">KODE</td>
            <td width="250">NAMA JAM SEWA</td>
            <td width="200"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td><span id="sprytextfield1">
              <label>
                <input name="kode_jam_sewa" type="text" id="kode_jam_sewa" value="<?php echo $row_dws['kode_jam_sewa']; ?>" size="15" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><span id="sprytextfield2">
              <label>
                <input name="jam_sewa" type="text" id="jam_sewa" value="<?php echo $row_dws['jam_sewa']; ?>" size="35" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><label>
              <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
            <td align="center">Edit</td>
            <td align="center">Del</td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($dws);
?>
