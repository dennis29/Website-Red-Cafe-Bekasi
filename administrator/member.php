<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$currentPage = $_SERVER["PHP_SELF"];

$maxRows_MEM = 10;
$pageNum_MEM = 0;
if (isset($_GET['pageNum_MEM'])) {
  $pageNum_MEM = $_GET['pageNum_MEM'];
}
$startRow_MEM = $pageNum_MEM * $maxRows_MEM;

mysql_select_db($database_koneksi, $koneksi);
$query_MEM = "SELECT * FROM member ORDER BY kode_member ASC";
$query_limit_MEM = sprintf("%s LIMIT %d, %d", $query_MEM, $startRow_MEM, $maxRows_MEM);
$MEM = mysql_query($query_limit_MEM, $koneksi) or die(mysql_error());
$row_MEM = mysql_fetch_assoc($MEM);

if (isset($_GET['totalRows_MEM'])) {
  $totalRows_MEM = $_GET['totalRows_MEM'];
} else {
  $all_MEM = mysql_query($query_MEM);
  $totalRows_MEM = mysql_num_rows($all_MEM);
}
$totalPages_MEM = ceil($totalRows_MEM/$maxRows_MEM)-1;

$queryString_MEM = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_MEM") == false && 
        stristr($param, "totalRows_MEM") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_MEM = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_MEM = sprintf("&totalRows_MEM=%d%s", $totalRows_MEM, $queryString_MEM);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
<li class="TabbedPanelsTab" tabindex="0">Lihat Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
<div class="TabbedPanelsContent">&gt;&gt; Lihat Data<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="814" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="6" align="center" bgcolor="#EEEEEE"><h2>DATA MEMBER</h2></td>
          </tr>
          <tr>
            <td colspan="6"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="100">KODE</td>
            <td width="250">NAMA MEMBER</td>
            <td width="200">JENIS KELAMIN</td>
            <td width="200"><p>TELEPON</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_MEM['kode_member']; ?></td>
              <td><?php echo $row_MEM['nama_member']; ?></td>
              <td><?php echo $row_MEM['jenis_kelamin']; ?></td>
              <td><?php echo $row_MEM['telepon']; ?></td>
              <td align="center"><a href="memberedit.php?kode_member=<?php echo $row_MEM['kode_member']; ?>" target="_self">Edit</a></td>
              <td align="center"><a href="memberdelete.php?kode_member=<?php echo $row_MEM['kode_member']; ?>" target="_self">Del</a></td>
            </tr>
            <?php } while ($row_MEM = mysql_fetch_assoc($MEM)); ?>
<tr>
            <td colspan="6"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">
Records <?php echo ($startRow_MEM + 1) ?> to <?php echo min($startRow_MEM + $maxRows_MEM, $totalRows_MEM) ?> of <?php echo $totalRows_MEM ?></td>
            <td colspan="4" align="right">&nbsp;
              <table border="0">
                <tr>
                  <td><?php if ($pageNum_MEM > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_MEM=%d%s", $currentPage, 0, $queryString_MEM); ?>"><img src="First.gif" /></a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_MEM > 0) { // Show if not first page ?>
                      <a href="<?php printf("%s?pageNum_MEM=%d%s", $currentPage, max(0, $pageNum_MEM - 1), $queryString_MEM); ?>"><img src="Previous.gif" /></a>
                      <?php } // Show if not first page ?></td>
                  <td><?php if ($pageNum_MEM < $totalPages_MEM) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_MEM=%d%s", $currentPage, min($totalPages_MEM, $pageNum_MEM + 1), $queryString_MEM); ?>"><img src="Next.gif" /></a>
                      <?php } // Show if not last page ?></td>
                  <td><?php if ($pageNum_MEM < $totalPages_MEM) { // Show if not last page ?>
                      <a href="<?php printf("%s?pageNum_MEM=%d%s", $currentPage, $totalPages_MEM, $queryString_MEM); ?>"><img src="Last.gif" /></a>
                      <?php } // Show if not last page ?></td>
                </tr>
            </table></td>
          </tr>
        </table>
      </form>
    </div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($MEM);
?>
