<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO jenis_pembayaran (kode_jenis_pembayaran, nama_jenis_pembayaran) VALUES (%s, %s)",
                       GetSQLValueString($_POST['kode_jenis_pembayaran'], "text"),
                       GetSQLValueString($_POST['nama_jenis_pembayaran'], "text"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($insertSQL, $koneksi) or die(mysql_error());

  $insertGoTo = "jenis_pembayaran.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

$maxRows_DJP = 10;
$pageNum_DJP = 0;
if (isset($_GET['pageNum_DJP'])) {
  $pageNum_DJP = $_GET['pageNum_DJP'];
}
$startRow_DJP = $pageNum_DJP * $maxRows_DJP;

mysql_select_db($database_koneksi, $koneksi);
$query_DJP = "SELECT * FROM jenis_pembayaran ORDER BY kode_jenis_pembayaran ASC";
$query_limit_DJP = sprintf("%s LIMIT %d, %d", $query_DJP, $startRow_DJP, $maxRows_DJP);
$DJP = mysql_query($query_limit_DJP, $koneksi) or die(mysql_error());
$row_DJP = mysql_fetch_assoc($DJP);

if (isset($_GET['totalRows_DJP'])) {
  $totalRows_DJP = $_GET['totalRows_DJP'];
} else {
  $all_DJP = mysql_query($query_DJP);
  $totalRows_DJP = mysql_num_rows($all_DJP);
}
$totalPages_DJP = ceil($totalRows_DJP/$maxRows_DJP)-1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BANK OCBC</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
    <li class="TabbedPanelsTab" tabindex="0">Input Data</li>
    <li class="TabbedPanelsTab" tabindex="0">Lihat Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
    <div class="TabbedPanelsContent">&gt;&gt; Input Data <br />
      <form id="form2" name="form1" method="POST" action="<?php echo $editFormAction; ?>">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>INPUT DATA JENIS PEMBAYARAN</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="250">NAMA JENIS PEMBAYARAN</td>
            <td width="200"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td><span id="sprytextfield1">
              <label>
                <input name="kode_jenis_pembayaran" type="text" id="kode_jenis_pembayaran" size="15" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><span id="sprytextfield2">
              <label>
                <input name="nama_jenis_pembayaran" type="text" id="nama_jenis_pembayaran" size="35" />
              </label>
              <span class="textfieldRequiredMsg">A value is required.</span></span></td>
            <td><label>
              <input type="submit" name="button" id="button" value="Simpan" />
            </label></td>
            <td align="center">Edit</td>
            <td align="center">Del</td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
        <input type="hidden" name="MM_insert" value="form1" />
      </form>
    </div>
    <div class="TabbedPanelsContent">&gt;&gt; Lihat Data<br />
      <form id="form1" name="form1" method="post" action="">
        <table width="600" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40" colspan="5" align="center" bgcolor="#EEEEEE"><h2>DATA JENIS PEMBAYARAN</h2></td>
          </tr>
          <tr>
            <td colspan="5"><img src="images/toolsheaderatas.jpg" width="807" height="25" /></td>
          </tr>
          <tr bgcolor="#EDEDED">
            <td width="50">KODE</td>
            <td width="250">NAMA JENIS PEMBAYARANS</td>
            <td width="200"><p>KETERANGAN</p></td>
            <td width="25" align="center"><img src="images/edit.png" width="16" height="16" /></td>
            <td width="25" align="center"><img src="images/del.png" width="16" height="16" /></td>
          </tr>
          <?php do { ?>
            <tr bgcolor="#FFFFFF">
              <td><?php echo $row_DJP['kode_jenis_pembayaran']; ?></td>
              <td><?php echo $row_DJP['nama_jenis_pembayaran']; ?></td>
              <td>&nbsp;</td>
              <td align="center"><a href="jenis_pembayaran_edit.php?kode_jenis_pembayaran=<?php echo $row_DJP['kode_jenis_pembayaran']; ?>">Edit</a></td>
              <td align="center"><a href="jenis_pembayaran_delete.php?kode_jenis_pembayaran=<?php echo $row_DJP['kode_jenis_pembayaran']; ?>">Del</a></td>
            </tr>
            <?php } while ($row_DJP = mysql_fetch_assoc($DJP)); ?>
<tr>
            <td colspan="5"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
          </tr>
          <tr bgcolor="#D7D7D7">
            <td colspan="2">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1", {defaultTab:1});
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($DJP);
?>
