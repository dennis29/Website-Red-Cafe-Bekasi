<?php require_once('Connections/koneksi.php'); ?>
<?php require_once('Connections/koneksi.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "index.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($QUERY_STRING) && strlen($QUERY_STRING) > 0) 
  $MM_referrer .= "?" . $QUERY_STRING;
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE member SET nama_member=%s, jenis_kelamin=%s, telepon=%s, alamat1=%s, alamat2=%s WHERE kode_member=%s",
                       GetSQLValueString($_POST['nama_member'], "text"),
                       GetSQLValueString($_POST['jenis_kelamin'], "text"),
                       GetSQLValueString($_POST['telepon'], "text"),
                       GetSQLValueString($_POST['alamat1'], "text"),
                       GetSQLValueString($_POST['alamat2'], "text"),
                       GetSQLValueString($_POST['kode_member'], "int"));

  mysql_select_db($database_koneksi, $koneksi);
  $Result1 = mysql_query($updateSQL, $koneksi) or die(mysql_error());

  $updateGoTo = "member.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_MEM = "-1";
if (isset($_GET['kode_member'])) {
  $colname_MEM = $_GET['kode_member'];
}
mysql_select_db($database_koneksi, $koneksi);
$query_MEM = sprintf("SELECT * FROM member WHERE kode_member = %s", GetSQLValueString($colname_MEM, "int"));
$MEM = mysql_query($query_MEM, $koneksi) or die(mysql_error());
$row_MEM = mysql_fetch_assoc($MEM);
$totalRows_MEM = mysql_num_rows($MEM);

$currentPage = $_SERVER["PHP_SELF"];

$queryString_MEM = "";
if (!empty($_SERVER['QUERY_STRING'])) {
  $params = explode("&", $_SERVER['QUERY_STRING']);
  $newParams = array();
  foreach ($params as $param) {
    if (stristr($param, "pageNum_MEM") == false && 
        stristr($param, "totalRows_MEM") == false) {
      array_push($newParams, $param);
    }
  }
  if (count($newParams) != 0) {
    $queryString_MEM = "&" . htmlentities(implode("&", $newParams));
  }
}
$queryString_MEM = sprintf("&totalRows_MEM=%d%s", $totalRows_MEM, $queryString_MEM);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 11px;
}
body {
	background-color: #CCF;
}
-->
</style>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="TabbedPanels1" class="TabbedPanels">
  <ul class="TabbedPanelsTabGroup">
<li class="TabbedPanelsTab" tabindex="0">Edit Data</li>
</ul>
  <div class="TabbedPanelsContentGroup">
<div class="TabbedPanelsContent">&gt;&gt; Lihat Data<br />
  <form action="<?php echo $editFormAction; ?>" id="form1" name="form1" method="POST">
    <table width="800" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td height="35" colspan="2" align="center"><h2>EDIT DATA MEMBER</h2></td>
      </tr>
      <tr>
        <td colspan="2"><img src="images/toolsheaderatas.jpg" width="805" height="25" /></td>
      </tr>
      <tr>
        <td width="200">Kode Member</td>
        <td width="600"><label>
          <input name="kode_member" type="text" id="kode_member" value="<?php echo $row_MEM['kode_member']; ?>" size="15" />
        </label></td>
      </tr>
      <tr>
        <td>Nama Member</td>
        <td><span id="sprytextfield1">
          <label>
            <input name="nama_member" type="text" id="nama_member" value="<?php echo $row_MEM['nama_member']; ?>" size="25" />
            </label>
          <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
      <tr>
        <td>Jenis Kelamin</td>
        <td><span id="spryselect1">
          <label>
            <select name="jenis_kelamin" id="jenis_kelamin">
              <option value="" <?php if (!(strcmp("", $row_MEM['jenis_kelamin']))) {echo "selected=\"selected\"";} ?>>Pilih Jenis Kelamin</option>
              <option value="Laki-Laki" <?php if (!(strcmp("Laki-Laki", $row_MEM['jenis_kelamin']))) {echo "selected=\"selected\"";} ?>>Laki-Laki</option>
              <option value="Perempuan" <?php if (!(strcmp("Perempuan", $row_MEM['jenis_kelamin']))) {echo "selected=\"selected\"";} ?>>Perempuan</option>
              </select>
            </label>
          <span class="selectRequiredMsg">Please select an item.</span></span></td>
</tr>
      <tr>
        <td>Telepon</td>
        <td><span id="sprytextfield2">
          <label>
            <input name="telepon" type="text" id="telepon" value="<?php echo $row_MEM['telepon']; ?>" />
            </label>
          <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
      <tr>
        <td>Alamat</td>
        <td><span id="sprytextfield3">
          <label>
            <input name="alamat1" type="text" id="alamat1" value="<?php echo $row_MEM['alamat1']; ?>" size="50" />
            </label>
          <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
      <tr>
        <td>&nbsp;</td>
        <td><span id="sprytextfield4">
          <label>
            <input name="alamat2" type="text" id="alamat2" value="<?php echo $row_MEM['alamat2']; ?>" size="45" />
            </label>
          <span class="textfieldRequiredMsg">A value is required.</span></span></td>
</tr>
      <tr>
        <td>&nbsp;</td>
        <td><label>
          <input type="submit" name="button" id="button" value="Simpan" />
        </label></td>
      </tr>
      <tr>
        <td colspan="2"><img src="images/toolsheaderbawah.jpg" width="807" height="30" /></td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table>
    <input type="hidden" name="MM_insert" value="form1" />
    <table width="800" border="1" cellspacing="0" cellpadding="0">
      <tr> </tr>
      <tr> </tr>
      <tr> </tr>
      <tr> </tr>
      <tr> </tr>
    </table>
    <input type="hidden" name="MM_update" value="form1" />
    <input type="hidden" name="MM_update" value="form1" />
  </form>
</div>
</div>
</div>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
//-->
</script>
</body>
</html>
<?php
mysql_free_result($MEM);

mysql_free_result($MEM);
?>
