<?php
//initialize the session
if (!isset($_SESSION)) {
  session_start();
}

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  $_SESSION['MM_Username'] = NULL;
  $_SESSION['MM_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['MM_Username']);
  unset($_SESSION['MM_UserGroup']);
  unset($_SESSION['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Program Aplikasi Penyewaan</title>
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body,td,th {
	font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
body {
	background-color: #638BBF;
}
-->
</style></head>

<body>
<table width="170" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" bgcolor="#8DADD3"><img src="images/Menu_Utama.jpg" width="170" height="35" /></td>
  </tr>
  <tr>
    <td><div id="Accordion1" class="Accordion" tabindex="0">
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">Home</div>
        <div class="AccordionPanelContent">&gt;&gt; <a href="home.php" target="_top">Menu Utama</a><br />
&gt;&gt; <a href="<?php echo $logoutAction ?>" target="_top">Logout</a></div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">Entry Master</div>
        <div class="AccordionPanelContent">&gt;&gt; <a href="member.php" target="mainFrame">Member</a><br />
          &gt;&gt; <a href="tempat.php" target="mainFrame">Tempat</a><br />
          &gt;&gt; <a href="waktu_sewa.php" target="mainFrame">Waktu Sewa</a><br />
          &gt;&gt; <a href="jenis_pembayaran.php" target="mainFrame">Jenis Pembayaran</a><br />
          &gt;&gt; <a href="jadwal.php" target="mainFrame">Jadwal</a></div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">Transaction</div>
        <div class="AccordionPanelContent">&gt;&gt; <a href="penyewaan.php" target="mainFrame">Penyewaan</a></div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">Report</div>
        <div class="AccordionPanelContent">&gt;&gt; <a href="laporan_member.php" target="mainFrame">Laporan Member<br />
        </a>          &gt;&gt; <a href="laporan_penyewaan.php" target="mainFrame">Laporan Penyewaan</a><br />
        </div>
      </div>
      <div class="AccordionPanel">
        <div class="AccordionPanelTab">Utility</div>
        <div class="AccordionPanelContent">&gt;&gt; Password</div>
      </div>
    </div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
</body>
</html>