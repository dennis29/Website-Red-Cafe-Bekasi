﻿var nav_unpin_duration = 1000;
var nav_pin_duration = 800;

$(function () {

   
    $("#main-navigation").scrollUpMenu();

    $("#dl-mobile-menu").dlmenu({animationClasses : { in : 'dl-animate-in-2', out : 'dl-animate-out-2' },
        trigger: '.dl-trigger-nav' });
    $("#dl-mobile-menu-search").dlmenu({ trigger: '.dl-trigger-search' });
   

    $(".dl-trigger-location").click(function () { window.location = "http://www.hardrock.com/locations.aspx"; return false;});
    
    var mobileMainMenu = $.data($("#dl-mobile-menu")[0], 'dlmenu');
    var mobileSearchMenu = $.data($("#dl-mobile-menu-search")[0], 'dlmenu');    
    
    //$("#mobile-menu-container button.dl-trigger-nav").click(function () {
    //    if (mobileMainMenu != 'undefined')
    //        mobileMainMenu._closeMenu();
    //    return true;
    //});

    //$("#mobile-menu-container button.dl-trigger-search").click(function () {
    //    if (mobileSearchMenu != 'undefined')
    //        mobileSearchMenu._closeMenu();
    //    return true;
    //});
    
    $('.search-btn').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            SubmitSearch();
        }
    });

    
    
    CenterCarouselImages();

    $(window).resize(function () {
        CenterCarouselImages();
    });
      
    $(".search-btn").click(function () { SearchBoxEnable(); });
    $(".search-btn-go").click(function () { SubmitSearch(); });

    $('#mobile-menu-search').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            SubmitSearch("#mobile-menu-search");
        }
    });


    $('.search-btn').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            SubmitSearch();
        }        
    });

    $('#footer-email-input').keydown(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            EmailSubmit('#footer-email-input');
        }
    });

    $(".footer-utility-link a").each(function () {
        var href = $(this).attr("href");
        var url = document.URL.replace("http://", "").replace("https://", "");
        
        href = href.replace("{CURRENTURL}", url);
        $(this).attr("href", href);
    });
   


    function SubmitSearch(sel) {
        if (sel == null)
            sel = ".search-btn";
        var strSearch = $(sel).val();
        if (strSearch != "") {
            window.location = '/search/default.aspx?searchString=' + URLEncode(strSearch, true);
        }
    }
    


    $('#top-carousel').carousel({
        interval: 5000
    });


    $('#promo-carousel').carousel({
        interval: 6000
    });
      

  

   

    /*
    var randNum = Math.floor((Math.random() * 2) + 1);
     //init controller
    controller = new ScrollMagic();
    // build tween
    var twnNavLogo = TweenMax.to(".home-page #nav-logo", 1, { scale: .8 });
    var twnNavMain = TweenMax.to("#navbar-main", 1, { className: "+=shrink" });
    var twnNavTop = TweenMax.to("#navbar-top", 1, { className: "+=shrink" });
    var twnNavLinks = TweenMax.to("#navbar-main .navbar-nav > li > a", 1, { className: "+=shrink" });    

 
    // build scene    
    controller.addScene([
        scnNavMain = new ScrollScene({ duration: 190, offset: 0 })
                   .setTween(twnNavMain),
        scnNavTop = new ScrollScene({ duration: 100, offset: 0 })
                   .setTween(twnNavTop),

        
        scnNavLinks = new ScrollScene({ duration: 400, offset: 0 })
                   .setTween(twnNavLinks),
        scnNavLogo = new ScrollScene({ duration: 400, offset: 0 })
                   .setTween(twnNavLogo)
 
    ]);
    
   */

    // show indicators (requires debug extension)
    //scene.addIndicators();
    //keeps the top level nav item active while browing the dropdown
    $(".dropdown-menu").hover(function () {
        $(this).parents("li.dropdown").addClass("active");
    }, function () {
        $(this).parents("li.dropdown").removeClass("active");
    });

    //remove the css-drop to disable css dropdowns
    $(".css-drop").addClass("js-drop").removeClass("css-drop");    
    
    //on hover show dropdowns
    $(".js-drop").hoverIntent(function () {
        $(this).find(".dropdown-menu").finish().show(500);
    }, function () {        
        $(this).find(".dropdown-menu").finish().hide(500);
    });

    //on hover expand the main container to fit the dropdown
    var navBarMinHeight = $(".navbar-main").css("minHeight");
    $(".navbar-main .navbar-nav").hoverIntent(function () {
        var navBarNewMinHeight = parseInt(navBarMinHeight.replace("px", "")) + 130;
        $(".navbar-main").finish().animate({ minHeight: navBarNewMinHeight + "px" });
    }, function () {
        $(".navbar-main").finish().delay(200).animate({ minHeight: navBarMinHeight });
    });
});

function CenterCarouselImages() {
    var sliderWidth = $(".carousel-inner").width();
    var imgWidth = 1800;
    var imgLeft = ((imgWidth - parseInt(sliderWidth)) / 2) * -1;
    $(".carousel-inner .item img").css("left", imgLeft + "px");
}

function EmailSubmit(sel) {
    var email = $(sel).val();
    window.location = "http://www.hardrock.com/rewards/?email=" + email;
}

function FindLocation(sel) {
    var query = $(sel).val();

    //backwards compatibility for sending just a query instead of a selector.
    if ($(sel).length == 0 && sel.indexOf("#") == -1 && sel.indexOf(".") == -1) {
        query = sel;
    }
    window.location = "http://www.hardrock.com/locations.aspx?q=" + query;
}

function SearchBoxEnable() {
    var twnSearchBox = TweenMax.to(".search-btn", 1, { width: 180, className: "+=active", ease: Quint.easeOut, onComplete: function () { SearchBoxButtonShow();} });
    $(".search-btn").blur(function () { SearchBoxDisable(); });
}

function SearchBoxButtonShow() {
    $(".search-btn-go").fadeIn();
}

function SearchBoxButtonHide() {
    $(".search-btn-go").fadeOut();
}

function SearchBoxDisable() {
 
    TweenMax.to(".search-btn", 1, { width: 98, ease: Quint.easeIn });
    TweenMax.to(".search-btn", 1, { className: "-=active", delay: 1.5 });
    SearchBoxButtonHide();
}

function URLEncode(input, urlChars) {
    var s = encodeURI(input);
    s = s.replace(" ", "%20");
    if (urlChars) {
        s = s.replace("&", "%26");
        s = s.replace("?", "%3F");
        s = s.replace("=", "%3D");
    }
    return s;
}


/**
 *  Project:      Scroll Up For Menu
 *  Description:  A simple mobile optimised menuing system which gets out of the way when you're not using it.
 *  Author:       David Simpson <david@davidsimpson.me>
 *  License:      Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>
 *  Source:       http://github.com/dvdsmpsn/scroll-up-for-menu
 *
 *  Usage:        $('#top').scrollUpForMenu(options);
 *      
 *
 *
 */
; (function ($, window, document, undefined) {

    var pluginName = "scrollUpMenu";
    var defaults = {
        waitTime: 10,
        transitionTime: 250,
        menuCss: { 'position': 'fixed', 'top': '0' }
    };

    var lastScrollTop = 0;
    var $header;
    var timer;
    var pixelsFromTheTop;

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {

            var self = this;
            $header = $(this.element);
            $header.css(self.settings.menuCss);
            pixelsFromTheTop = $header.height();

            $header.next().css({ 'margin-top': pixelsFromTheTop });

            $(window).bind('scroll', function () {
                clearTimeout(timer);
                timer = setTimeout(function () {
                    self.refresh(self.settings)
                }, self.settings.waitTime);
            });
        },
        refresh: function (settings) {
            // Stopped scrolling, do stuff...				   			
            var scrollTop = $(window).scrollTop();

            if (scrollTop > lastScrollTop && scrollTop > pixelsFromTheTop) { // ensure that the header doesnt disappear too early
                // downscroll
                $header.slideUp(settings.transitionTime);
                $header.addClass("nav-not-top");
            } else {
                // upscroll
                $header.slideDown(settings.transitionTime);

                if (scrollTop <= pixelsFromTheTop) {
                    $header.removeClass("nav-not-top");
                }
            }
            lastScrollTop = scrollTop;
        }
    };

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);