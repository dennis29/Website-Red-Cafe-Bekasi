
objWT = new HRCWT();

/* Begin Editable */
//objWT.add(URL SEARCH KEY, CONTENT GROUP NAME, [CONTENT SUBGROUP NAME]);

objWT.add('/live2/','Live');
objWT.add('/music/','Live');
objWT.add('/279/','Live');

//membership
objWT.add('estore/login/login.asp','Memberships','Login');
objWT.add('hrc_myrock_homepage.asp','MyRock');
objWT.add('services/customer/HRC_Register_1','MyRock','Registration');
objWT.add('estore/pinclub','Pinclub',null); 
objWT.add('all-access','AllAccess',null); 

//Philanthropy
objWT.add('philanthropy/our-mission', 'Corporate', 'Philanthropy');
objWT.add('philanthropy/partners', 'Corporate', 'Philanthropy');
objWT.add('philanthropy/signature-series', 'Corporate', 'Philanthropy');
objWT.add('philanthropy', 'Corporate', 'Philanthropy');

objWT.add('community/ambassador', 'Corporate', 'Philanthropy');
objWT.add('community/save_planet', 'Corporate', 'Philanthropy');
objWT.add('community/crank_it_up', 'Corporate', 'Philanthropy');

//corporate
objWT.add('corporate/ownership', 'Corporate');
objWT.add('corporate/career', 'Corporate', 'Careers');
objWT.add('corporate/contactus', 'Corporate');
objWT.add('corporate/faq', 'Corporate');
objWT.add('corporate/logos', 'Corporate');
objWT.add('corporate/history', 'Corporate');
objWT.add('corporate/management', 'Corporate');
objWT.add('corporate/', 'Corporate');

//Cafes
//objWT.add('locations/cafes3/', 'Cafes', getQuerystring(window.location.href, "locationid"));


//Options
objWT.options.dcsuri = window.location.href;
objWT.options.sv = window.location.hostname;
objWT.options.debug = false;


/* End Editable */



objWT.init();

function getQuerystring(href, key) {
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)","i");
    var qs = regex.exec(href);
    if (qs == null)
        return "";
    else
        return qs[1];
};

 function HRCWT(){
    this.WTArray = new Array();
   
    this.options = {
        dcsuri: "/",
        sv: "hardrock.com",
        debug: false
    }
   
    this.add = function(UrlKey, CGName, CGSub){
        var wt = {
            urlkey: UrlKey, 
            cgname: CGName, 
            cgsubname: CGSub
        };        
        this.WTArray.push(wt);        
    };

    this.init = function() {
       
        var strURL = window.location.pathname;
        var strWTJS = "";

        for (i = 0; i < this.WTArray.length; i++) {
            tmpWT = this.WTArray[i];

            var pattern = new RegExp(tmpWT.urlkey, "i");

            if (pattern.test(strURL)) {

                if (tmpWT.cgsubname) {
                    strWTJS = "dcsMultiTrack('DCS.dcsuri', '" + this.options.dcsuri + "', 'WT.sv', '" + this.options.sv + "','WT.cg_n','" + tmpWT.cgname + "', 'WT.cg_s','" + tmpWT.cgsubname + "'" + this.getAds() + ");";

                } else {
                    strWTJS = "dcsMultiTrack('DCS.dcsuri', '" + this.options.dcsuri + "', 'WT.sv', '" + this.options.sv + "','WT.cg_n','" + tmpWT.cgname + "'" + this.getAds() + ");";
                }

                if (this.options.debug) { alert("CGName: " + tmpWT.cgname + ((tmpWT.cgsubname) ? " - CGSubName: " + tmpWT.cgsubname : "")) };

                break;
            }
        }

        //no content group so lets just load ads if any

        if (strWTJS == "") {
            strWTJS = "dcsMultiTrack('DCS.dcsuri', '" + this.options.dcsuri + "', 'WT.sv', '" + this.options.sv + "'" + this.getAds() + ");";
        }

        eval(strWTJS);

        if (this.options.debug) alert(strWTJS);

        return;
    };

    function isInArray(strvalue, objArray){
        for (var x = 0; x < objArray; x++) {
            if (objArray[x] == strvalue) return true;
        }

        return false;
    }
    this.getAds = function () {
        return "";
        /*
        var arryWT = new Array();
        var imageNameExclusions = new Array("", "undefined", "dot_clear");
        var strWTAds = "";

        var els = document.getElementsByTagName("a");

        for (var i = 0; i < els.length; i++) {
            if (els[i].href.indexOf("src=") >= 0) {
                var strHref = els[i].href;
                var strVal = getQuerystring(strHref, "src");
                if (els[i].getElementsByTagName("img").length == 0) continue;

                var strImageName = getImageName(els[i].getElementsByTagName("img")[0].src);

                if (isInArray(strImageName, imageNameExclusions)) continue;

                if (strHref.indexOf("WT.mc_id") == -1) {
                    els[i].href = strHref + "&WT.mc_id=" + strImageName + "&WT.ac=" + strVal;
                }

                if (!isInArray(strVal, arryWT)) {
                    arryWT.push(strVal);
                }
            }
        }

        //return arryWT;


        for (var x = 0; x < arryWT.length; x++) {
            strWTAds += ",'WT.ad','" + arryWT[x] + "'";
        }

        //if (this.options.debug) alert(strWTAds);
        return strWTAds;
        //eval("dcsMultiTrack('DCS.dcsuri','/'" + strWTAds +");");
        */
    };


    var getImageName = function(str) {
        var regex = /[^\/]*$/;
        var s = regex.exec(str);

        if (s == null)
            return "";
        s = s[0];
        var dot = s.lastIndexOf(".");
        if (dot == -1) { return s; }
        s = s.substr(0, dot);
        return s;
    };

   
}


