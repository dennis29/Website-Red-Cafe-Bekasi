function openRemote(url) {
    var windWidth;
    var windHeight;
    windWidth = (window.screen.width / 2) - (320 + 5);
    windHeight = (window.screen.height / 2) - (200 + 25);
    poster = window.open(url, "map", "scrollbars=no,resizable=no,menubar=no,status=no,toolbar=no,location=no,directories=no,width=692,height=338, left=" + windWidth + ",top=" + windHeight + ",screenX=" + windWidth + ",screenY=" + windHeight + " ");
}

function submitSearch() {
    var strSearch = $('.searchBox .searchText').val();


    if (strSearch != 'Search' && strSearch != "") {
        window.location = '/search/default.aspx?searchString=' + URLEncode(strSearch, true);
    }
}

function URLEncode(input, urlChars) {
    var s = encodeURI(input);
    s = s.replace(" ", "%20");
    if (urlChars) {
        s = s.replace("&", "%26");
        s = s.replace("?", "%3F");
        s = s.replace("=", "%3D");
    }
    return s;
}

///duplicate function
//function FindLocation(query) {
//    if (query != null && query != "") {
//        $('#findLocationLoading').show();
//        window.location = '/locations.aspx?q=' + query;
//    }
//}


function openEventWindow(url) {
    return openNewsPopup(url);
}

function openNewsWindow(url) {
    return openNewsPopup(url);
} 

function openNewsPopup(url) {
    var usePopup = false;
    if ($("#newsPopup").get(0) == null) {
        $(".mainContainer").append("<div id='newsPopup'></div>");
    }
    var eventPopup = $('#newsPopup');
    if (usePopup = eventPopup.is(':visible')) {
        $("#newsPopup").html("<iframe id='ifrmNewsItem' allowtransparency='true' height='635' width='577' scrolling='no'  src='" + url + "' frameborder='0' style='overflow:hidden;' seamless='seamless'></iframe>");
        $("#newsPopup").modal({ onClose: closeNewsPopup });

        initScrollers();
    }
    return !usePopup;
}

function closeNewsPopup(url) {
    $.modal.close();
    $("#newsPopup").remove();

}

function openLiveEventWindow(url) {
    var usePopup = false;
    if ($("#liveEventPopup").get(0) == null) {
        $(".mainContainer").append("<div id='liveEventPopup'></div>");
    }
    var eventPopup = $('#liveEventPopup');
    if (usePopup = eventPopup.is(':visible')) {
        $("#liveEventPopup").html("<iframe id='ifrmNewsItem' allowtransparency='true' height='645' width='577' scrolling='no'  src='" + url + "' frameborder='0' style='overflow:hidden;' seamless='seamless'></iframe>");
        $("#liveEventPopup").modal({ onClose: closeLiveEventPopup });
    }

    return !usePopup;
}

function closeLiveEventPopup() {
    $.modal.close();
    $("#liveEventPopup").remove();
}

function openCafeEventWindow(url) {
    var usePopup = false;
    if ($("#cafeEventPopup").get(0) == null) {
        $(".mainContainer").append("<div id='cafeEventPopup'></div>");
    }
    var eventPopup = $('#cafeEventPopup');
    if (usePopup = eventPopup.is(':visible')) {
        $("#cafeEventPopup").html("<iframe id='ifrmNewsItem' allowtransparency='true' height='645' width='577' scrolling='no'  src='" + url + "' frameborder='0' style='overflow:hidden;' seamless='seamless'></iframe>");
        $("#cafeEventPopup").modal({ onClose: closeLiveEventPopup });
    }

    return !usePopup;
}

function closeCafeEventPopup() {
    $.modal.close();
    $("#cafeEventPopup").remove();
}

function openDiscountWindow() {
    var url = "/locations/cafes3/discounts.aspx";
    window.open(url, 'discounts', 'menubar=yes,width=435,height=720,scrollbars=no');
}

function TextSignup(el) {
    if (el.value == "Enter your email address") {
        el.value = "";
        $(el).css("color", "#000000");
    } else if (el.value == "") {
        el.value = "Enter your email address";
        $(el).css("color", "");
    }
}

function scrollToElement(el) {
    var target_offset = $(el).offset();
    var target_top = target_offset.top;
    //goto that anchor by setting the body scroll top to anchor top
    $('html, body').animate({ scrollTop: target_top - 45 }, 500);
}

function OpenWindow(strPage, strName, strProps) {

    if (strProps == 'undefined') {
        strProps = 'height=400,width=405,status=no,menu=no,scrollbars=yes,address=no';
    }
    window.open(strPage, strName, strProps);
}

var TimerFindLocation = null;
function showFindLocation() {
    $(".footerFindLocation").slideToggle(
        function () {
            var isVisible = ($(".footerFindLocation").is(":visible"));
            if (isVisible) {
                //TimerFindLocation = setTimeout("hideFindLocation()", 3000);
                //$(".footerFindLocation").hover(function () {
                //    clearTimeout(TimerFindLocation); 
                //}, function () {                    
                //    hideFindLocation();
                //});


                // $(".footerFindLocation *").hover(function () {
                //     clearTimeout(TimerFindLocation);
                // }, function () {
                //     //hideFindLocation();
                // });
            }
        }
    );
}

function hideFindLocation() {
    $(".footerFindLocation").slideUp();
}

function animateMapIcon() {

    if ($('#imgMapIcon').length == 0) {
        return;
    }

    if ($(".findLocationBoxLarge").length > 0) {
        maxLeft = 215;
    } else {
        maxLeft = 180;
    }
    var i;

    if ($('#imgMapIcon').css("left") == "0px") {
        i = maxLeft + 'px';
    } else {
        i = '0px';
    }

    $('#imgMapIcon').animate({
        left: i
    }, 5000, function () { animateMapIcon(); });
}

var textboxDefaultVal = new Array();

function getTextboxDefaultVal(el) {

    var val = $(el).val();
    if (textboxDefaultVal[$(el).attr("id")] != 'undefined' && textboxDefaultVal[$(el).attr("id")] != null) {
        val = textboxDefaultVal[$(el).attr("id")];
    } else if ($(el).attr("title") != "") {
        val = $(el).attr("title");
    }
    return val;
}
function textBoxFocus(el) {
    if ($(el).val() == getTextboxDefaultVal(el) || getTextboxDefaultVal(el) == null) {
        var defaultVal = getTextboxDefaultVal(el);
        textboxDefaultVal[$(el).attr("id")] = defaultVal;
        $(el).val("");
    }
}

function textBoxBlur(el) {
    if ($(el).val() == "") {
        var defaultVal = getTextboxDefaultVal(el);
        $(el).val(defaultVal);
    }
}

function handleEnterKey(e, button, func) {

    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //firefox 

    if (key == 13) {
        if (button != null && $(button).length > 0) {
            $(button).click();
            return false;
        }
        if (func != null) {
            func();
            return false;
        }

    } else {
        return true;
    }
}




function hoverOn(el) {
    if ($(el).attr('src').match(/.*_over.*/)) {
        return;
    }
    var imgOver = $(el).attr('src').replace(/(.*)\.(gif|jpg|png)/gi, "$1_over.$2");
    $(el).attr('src', imgOver);

}

function hoverOff(el) {
    var imgCurrent = $(el).attr('src').replace(/(.*)_over\.(gif|jpg|png)/gi, "$1.$2");
    $(el).attr('src', imgCurrent);
}

//dom ready functions
$(function () {


    //fix for floating bottom bar 
    if (navigator != null) {
        if (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod') {
            $("#hrcBottomBar").css("position", "static");
            $(".bottomBarBG").css("position", "static");
        };
    }

    //location list dropdown



    $(".LocationListControl select option[value='none']").attr('selected', 'selected');

    $(".LocationListControl select").change(function () {
        if ($(".LocationListControl select").val() != 'none') {
            location.href = $(".LocationListControl select").val();
        }
    });

    //no longer used since redesign
    //LoadFooterLocations();

    //no longer used since redesign/
    /*
    function LoadFooterLocations() {
        if ($("body#HRCPC").length > 0) return;

        $.ajax({
            type: "GET",
            url: "http://www.hardrock.com/webservices/WSLocation.asmx/GetAllLocationsJSON",

            dataType: "jsonp",

            success: function (data) {


                $.each(data, function (key, val) {
                    $(".locationList").append("<span class='location " + val.VenueType + "'><a href='" + val.WebPage + "' onclick=\"dcsMultiTrack(\'DCS.dcsuri\', \'" + val.WebPage + "\',\'WT.ti\', \'" + val.Name + "\');\" class='locationLink'>" + val.Name + "</a></span>");
                });
            },
            complete: function () {
               
                initScrollers(".locationListScrollpane");
                $('.footerFindLocation').css("display", "none").css("visibility", "visible");

            }
        });
    }
    */

    //initScrollers(".locationListScrollpane");

    initDefaultFields();
    initHovers();

    //initialize scrollers
    initScrollers(".scrollpane");

    //Preload all hover images
    $("img.hover,input.hover").each(function () {

        var imgSrc = $(this).attr('src');
        if (imgSrc != null) {
            var imgOver = imgSrc.replace(/(.*)\.(gif|jpg|png)/gi, "$1_over.$2");
            jQuery("<img>").attr("src", imgOver);
        }
    });


    //no longer used since redesign
    //$('body#HRCPC .footerFindLocation').css("display", "none").css("visibility", "visible");
     //$('.footerLocationListContainer').css("visibility", "hidden");

    //no longer used since redesign
    //animateMapIcon();


    $(".footerLink").each(function () {
        var href = $(this).attr("href");
        var url = document.URL.replace("http://", "").replace("https://", "");
        //for testing
        url = url.replace("staging-", "");
        href = href.replace("{CURRENTURL}", url);
        $(this).attr("href", href);
    });

});

function initDefaultFields() {

    $("input.default").each(function () {
        if ($(this).attr("title") == null) {
            $(this).attr("title", $(this).val())
        }

    });

    $("input.default").focus(function () {
        textBoxFocus(this);
    });

    $("input.default").blur(function () {
        textBoxBlur(this);
    });

}

function initScrollers(sel) {

    if (sel == null) sel = ".scrollpane";
    //initialize scrollers
    $(sel).jScrollPane({ scrollbarWidth: 9, showArrows: true, verticalDragMinHeight: 40, verticalDragMaxHeight: 231, mouseWheelSpeed: 30 });
    $(sel).css("visibility", "visible");
}

function initHovers() {
    //Add mouseover to all images with "hover" as classname  
    $("img.hover, input.hover").hover(function () {
        hoverOn($(this));

    }, function () {
        hoverOff($(this));
    });


    $("a.ahover").hover(function () {
        hoverOn($(this).find("img"));
    }, function () {
        hoverOff($(this).find("img"));
    });
}

function initNewsLinks() {
    $(".newsLinksPopup a").each(function () {
        $(this).click(function () {
            window.open($(this).attr("href").replace("/locations/cafes3/events.aspx", "/live2/eventDetails.aspx"), 'ViewEvent', 'menubar=yes,width=420,height=500,scrollbars=yes');
            return false;
        });
    });
}

/*validation addititions*/

ValidatorCommonOnSubmit = function () {
    ClearValidatorCallouts();
    var result = SetValidatorCallouts();
    return result;
}

ValidatorValidate = function (val, validationGroup, event) {
    val.isvalid = true;
    if ((typeof (val.enabled) == 'undefined' || val.enabled != false) && IsValidationGroupMatch(val, validationGroup)) {
        if (typeof (val.evaluationfunction) == 'function') {
            val.isvalid = val.evaluationfunction(val);
            if (!val.isvalid && Page_InvalidControlToBeFocused == null && typeof (val.focusOnError) == 'string' && val.focusOnError == 't') {
                ValidatorSetFocus(val, event);
            }
        }

    }

    ClearValidatorCallouts();
    SetValidatorCallouts();
    ValidatorUpdateDisplay(val);
}

SetValidatorCallouts = function () {
    var i;
    var pageValid = true;
    for (i = 0; i < Page_Validators.length; i++) {
        var inputControl = document.getElementById(Page_Validators[i].controltovalidate);
        if (!Page_Validators[i].isvalid) {
            if (pageValid)
                inputControl.focus();
            WebForm_AppendToClassName(inputControl, 'error');
            pageValid = false;
        }
    }

    return pageValid;
}

ClearValidatorCallouts = function () {
    var i;
    var invalidConrols = [];
    for (i = 0; i < Page_Validators.length; i++) {
        var inputControl = document.getElementById(Page_Validators[i].controltovalidate);
        WebForm_RemoveClassName(inputControl, 'error');
    }
}

function WebForm_RemoveClassName(element, className) {
    var current = element.className;
    if (current) {
        if (current.substring(current.length - className.length - 1, current.length) == ' ' + className) {
            element.className = current.substring(0, current.length - className.length - 1);
            return;
        }

        if (current == className) {
            element.className = "";
            return;
        }

        var index = current.indexOf(' ' + className + ' ');
        if (index != -1) {
            /* BUG 1: index + 1 instead of index to include one space */
            element.className = current.substring(0, index + 1) + current.substring(index + className.length + 2, current.length);
            return;
        }

        /* BUG 2: className.length + 1 instead of className.length in order that the comparison is true */
        if (current.substring(0, className.length + 1) == className + ' ')
            element.className = current.substring(className.length + 1, current.length);
    }
}
function WebForm_AppendToClassName(element, className) {
    var current = element.className;
    if (current) {
        if (current.charAt(current.length - 1) != ' ') {
            current += ' ';
        }
        current += className;
    }
    else {
        current = className;
    }
    element.className = current;
}





function Main_Load() {
    initRotator();
}



function initRotator(rotator) {
    if (rotator == null && typeof $find == 'function') {
        rotator = $find(rotatorMainID);
    }
    stopRotator(null, rotator);
    startRotator(rotator, Telerik.Web.UI.RotatorScrollDirection.Left);
}

function showNextItem(clickedButton, rotator, direction) {
    initRotator(rotator);
    rotator.showNext(direction);
}

function startRotator(rotator, direction) {
    if (!rotator.autoIntervalID) {
        rotator.autoIntervalID = window.setInterval(function () {
            rotator.showNext(direction);
        }, rotator.get_frameDuration());
    }
}

function stopRotator(clickedButton, rotator) {
    if (rotator.autoIntervalID) {
        window.clearInterval(rotator.autoIntervalID);
        rotator.autoIntervalID = null;
    }
}



/* jquery extensions */
jQuery.fn.swapWith = function (to) {
    return this.each(function () {
        var copy_to = $(to).clone(true);
        $(to).replaceWith(this);
        $(this).replaceWith(copy_to);
    });
};


jQuery.fn.center = function (options) {
    var options = $.extend({ // Default values
        parent: window, // element, center into window  
        offsetTop: 0,
        offsetLeft: 0
    }, options);


    this.css("position", "absolute");
    this.css("top", (($(options.parent).height() - this.outerHeight()) / 2 + $(window).scrollTop() + options.offsetTop) + "px");
    this.css("left", (($(options.parent).width() - this.outerWidth()) / 2 + $(window).scrollLeft() + options.offsetLeft) + "px");
    return this;
};


(function ($) {
    $.fn.extend({
        hrcModal: function (options) {
            var options = $.extend({ // Default values
                parent: $("body"),
                windowId: "hrcModal",
                width: null,
                height: null,
                offsetLeft: 0,
                offsetTop: 0
            }, options);

            $(this).wrap("<div id=\"" + options.windowId + "\" class=\"modal-window\" style=\"z-index: 105; width:" + options.width + "px; height:" + options.height + "px; margin-top:-" + (options.height / 2) + "px; margin-left:-" + (options.width / 2) + "px;\" />");
            //$(this).wrap("<div id='" + options.windowId + "' class='modal-window' style='width:" + options.width + "px; height:" + options.height + "px;' />");
            var modal = $("#" + options.windowId)


            $(this).before("<div class=\"modal-overlay\"></div>");

            //modal.css("top", (($(options.parent).height() -  $(this).outerHeight()) / 2 + $(window).scrollTop() + options.offsetTop) + "px");
            //modal.css("left", (($(options.parent).width() -  $(this).outerWidth()) / 2 + $(window).scrollLeft() + options.offsetLeft) + "px");             
            //$("body").append("<div class=\"modal-overlay\"></div>");
            //$("body").prepend("<div class=\"modal-overlay\"></div>");
            //$('<p>Test</p>').insertBefore('.inner');

            $(".modal-overlay").click(function () { $(this).closeModal(); });
        },

        closeModal: function () {
            $(".modal-window").remove();
            $(".modal-overlay").remove();
        }

    });
})(jQuery);
