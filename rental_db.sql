-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 07, 2013 at 10:18 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rental_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE IF NOT EXISTS `jadwal` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jam` varchar(15) NOT NULL,
  `tempat` varchar(15) NOT NULL,
  `status_ruangan` varchar(10) NOT NULL DEFAULT 'Kosong',
  `harga_perjam` int(12) NOT NULL DEFAULT '0',
  `keterangan` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`ID`, `jam`, `tempat`, `status_ruangan`, `harga_perjam`, `keterangan`) VALUES
(1, '08.00 s/d 09.00', 'Ruangan 1', 'Kosong', 100000, ''),
(2, '09.00 s/d 10.00', 'Ruangan 1', 'Kosong', 100000, ''),
(3, '10.00 s/d 11.00', 'Ruangan 1', 'Kosong', 100000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jam_sewa`
--

CREATE TABLE IF NOT EXISTS `jam_sewa` (
  `kode_jam_sewa` int(11) NOT NULL AUTO_INCREMENT,
  `jam_sewa` varchar(25) NOT NULL,
  PRIMARY KEY (`kode_jam_sewa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `jam_sewa`
--

INSERT INTO `jam_sewa` (`kode_jam_sewa`, `jam_sewa`) VALUES
(1, '08.00 s/d 09.00'),
(2, '09.00 s/d 10.00'),
(3, '10.00 s/d 11.00'),
(4, '11.00 s/d 12.00'),
(5, '12.00 s/d 13.00'),
(6, '13.00 s/d 14.00'),
(7, '14.00 s/d 15.00');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE IF NOT EXISTS `jenis_pembayaran` (
  `kode_jenis_pembayaran` varchar(5) NOT NULL,
  `nama_jenis_pembayaran` varchar(20) NOT NULL,
  PRIMARY KEY (`kode_jenis_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`kode_jenis_pembayaran`, `nama_jenis_pembayaran`) VALUES
('001', 'Transfer'),
('002', 'Cash'),
('003', 'Kartu Kredit');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `kode_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(35) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `alamat1` varchar(50) NOT NULL,
  `alamat2` varchar(45) NOT NULL,
  PRIMARY KEY (`kode_member`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`kode_member`, `nama_member`, `jenis_kelamin`, `telepon`, `alamat1`, `alamat2`) VALUES
(1, 'Yadi Supriyadi', 'Laki-Laki', '081278658876', 'Jl.Pocong Kp Utan Bekasi Timur', 'Cikarang Selatan Bekasi');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(12) NOT NULL,
  `nama_pembayaran` varchar(30) NOT NULL,
  `untuk_pembayaran` varchar(30) NOT NULL,
  `sejumlah` int(11) NOT NULL,
  `keterangan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `penyewaan`
--

CREATE TABLE IF NOT EXISTS `penyewaan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(12) NOT NULL,
  `nama_member` varchar(35) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `nama_tempat` varchar(25) NOT NULL,
  `lama_sewa` int(11) NOT NULL DEFAULT '0',
  `mulai_jam` varchar(5) NOT NULL,
  `sampai_jam` varchar(5) NOT NULL,
  `harga_perjam` int(12) NOT NULL,
  `nama_jenis_pembayaran` varchar(20) NOT NULL,
  `total_harga` int(11) DEFAULT '0',
  `keterangan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `penyewaan`
--

INSERT INTO `penyewaan` (`ID`, `tanggal`, `nama_member`, `no_telepon`, `nama_tempat`, `lama_sewa`, `mulai_jam`, `sampai_jam`, `harga_perjam`, `nama_jenis_pembayaran`, `total_harga`, `keterangan`) VALUES
(1, '2013-12-01', 'Yana Suryana', '081289876543', 'Ruangan 1', 1, '10.00', '11.00', 100000, 'Cash', 0, 'Oke'),
(2, '2013-12-15', 'Rama Ramdani', '92287872', 'Ruangan 2', 1, '09.00', '10.00', 100000, 'Cash', 0, 'Ok'),
(3, '2013-12-07', 'Hendarman Sutejo', '081398765678', 'Ruangan 1', 1, '10.00', '11.00', 100000, 'Cash', 0, 'Ok');

-- --------------------------------------------------------

--
-- Table structure for table `tempat`
--

CREATE TABLE IF NOT EXISTS `tempat` (
  `kode_tempat` varchar(5) NOT NULL,
  `nama_tempat` varchar(25) NOT NULL,
  `harga_perjam` int(12) NOT NULL DEFAULT '100000',
  PRIMARY KEY (`kode_tempat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tempat`
--

INSERT INTO `tempat` (`kode_tempat`, `nama_tempat`, `harga_perjam`) VALUES
('001', 'Ruangan 1', 100000),
('002', 'Ruangan 2', 100000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `hakakses` varchar(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `username`, `password`, `hakakses`) VALUES
(1, 'admin', 'admin', '1'),
(2, 'user', 'user', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
