<h4><br><br><br><br>
            
            <p><strong>Pelayanan paket sewa sound system sbb :</strong></p>
<p><strong>Paket Harga Rp. 2.000.000.- :</strong></p>
<ul>
<li>Sound 2.000 watt</li>
<li>4 set Spesaker aktif 15”</li>
<li>1 set Mixer + Accesoris</li>
<li>2 unit Wired Microphone</li>
<li>1 unit Wireless Microphone</li>
<li>1 unit DVD Player</li>
<li>1 orang Operator</li>
<li>Kabel Instalasi</li>
</ul>
<p><strong>Paket Harga Rp. 3.000.000,- :</strong></p>
<ul>
<li>Sound 3.000 watt</li>
<li>6 set Speaker aktif 15”</li>
<li>4 Three Pod Speaker</li>
<li>1 set Mixer + Accesoris</li>
<li>2 unit Wired Microphone</li>
<li>2 unit Wireless Microphone</li>
<li>1 unit DVD Player</li>
<li>1 orang Operator</li>
<li>Kabel Instalasi</li>
</ul>
<p><strong>Paket Harga Rp. 5.000.000,- :</strong></p>
<ul>
<li>Sound FOH &amp; Monitor 5.000 watt</li>
<li>2 FOH Speaker aktif 15”</li>
<li>2 FOH Subwoofer 18”</li>
<li>4 Speaker Aktif 15”</li>
<li>1 set Mixer + Accessoris</li>
<li>3 unit Wired Microphone</li>
<li>2 unit Wireless Microphone</li>
<li>1 unit DVD Player</li>
<li>1 orang Operator</li>
<li>Kabel Instalasi</li>
</ul>
<p><strong>Paket Harga Rp. 10.000.000,- :</strong></p>
<ul>
<li>Sound FOH &amp; Monitor 10.000 watt</li>
<li>4 FOH Speaker aktif 15”</li>
<li>4 FOH Subwoofer 18”</li>
<li>5 Speaker Aktif 15”</li>
<li>1 set Mixer + Accessoris</li>
<li>4 unit Wired Microphone</li>
<li>2 unit Wireless Microphone</li>
<li>1 unit DVD Player</li>
<li>1 orang Operator</li>
<li>Kabel Instalasi</li>
</ul>
<p><strong>Paket Harga Rp. 20.000.000,- :</strong></p>
<ul>
<li>Sound FOH &amp; Monitor 20.000 watt</li>
<li>4 FOH Speaker aktif 15”</li>
<li>8 FOH Subwoofer 18”</li>
<li>7 Speaker Aktif 15”</li>
<li>1 set Mixer + Accessoris</li>
<li>5 unit Wired Microphone</li>
<li>2 unit Wireless Microphone</li>
<li>1 unit DVD Player</li>
<li>1 orang Operator</li>
<li>Kabel Instalasi</li>
</ul>
<p>&nbsp;</p>
<div id="sidebar-primary">
<div style="text-align: center;">
<span style="color: #45818e;"><span style="font-family: Georgia,&quot;Times New Roman&quot;,serif;"><span style="font-size: large;"><blink><b>Harga Sewa Studio</b></blink></span></span></span></div>
<br>
<ul>
<li>Per Jam = Rp 35,000</li>
</ul>
Notes :<br>
<ol>
<li>Shift discount hanya Rp. 150.000,- per 6 Jam</li>
<li>Khusus bagi Member 10 kali sewa free 1 jam berlaku untuk weekday</li>
<li>jam buka Studio mulai jam 09:00 s.d 22:00 weekend  s.d jam 00:00</li>
<br>
Sfesifikasi Alat :
<ol>
	<li>Ampli Gitar Marshal JCM-900 Cabinet</li>
	<li>Ampli Bass Hartke Cabinet</li>
	<li>Ampli Keyboard Roland KC -550</li>
	<li>Drum Pearl 22 Inchi</li>
	<li>Cymbal Nebule 20 Inchi</li>
	<li>Bass Gitar SDGR Custom</li>
	<li>Lead Gitar Gibson Les Paul Custom</li>
	<li>Lead Gitar Hollow Body Custom</li>
	<li>Keyboard Korg + Roland</li>
	<li>Gitar Effect Zoom GX-1 + Distortion</li>
	<li>Speaker JBL 18 Inchi</li>
	<li>Mixer Phonic/Behringer 24 Channel</li>
	<li>Software Presonus Studio One Pro</li>
</ol>
		</li></ul></div></h4>